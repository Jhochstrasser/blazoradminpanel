namespace blazoradminpanel.Services
{
    public class PizzaSalesState {
        public int PizzasSoldToday { get; set; }
    }
}