using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using blazoradminpanel.Data;
using blazoradminpanel.Models;

namespace blazoradminpanel.Controllers {
    [Route("specials")]
    [ApiController]
    public class SpecialsContoller : Controller
    {
        private readonly PizzaStoreContext _db;

        public SpecialsContoller(PizzaStoreContext db) {
            _db = db;
        }

        [HttpGet]
        public async Task<ActionResult<List<PizzaSpecial>>> GetSpecials() {
            return (await _db.Specials.ToListAsync()).OrderByDescending(s => s.BasePrice).ToList();
        }
    }
}