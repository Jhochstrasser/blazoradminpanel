using Microsoft.EntityFrameworkCore;
using blazoradminpanel.Models;

namespace blazoradminpanel.Data {
    public class PizzaStoreContext : DbContext
    {
        public PizzaStoreContext(DbContextOptions options) : base(options) 
        {
        }

        public DbSet<PizzaSpecial>? Specials { get; set; }
    }
}